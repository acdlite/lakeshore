<?php
/**
 * The template for the front page.
 */

$context = Lakeshore::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['title'] = $post->post_title;

Timber::render('front-page.twig', $context);