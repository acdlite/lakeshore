module.exports = (grunt) ->
  @initConfig
    pkg: grunt.file.readJSON 'package.json'
    compass:
      build:
        options:
          require: 'susy'
          sassDir: 'scss'
          cssDir: 'css'
          imagesDir: 'img'
          fontsDir: 'fonts'
          relativeAssets: true
          environment: 'production'
    coffee:
      build:
        files:
          'js/main.js': 'coffee/main.coffee'
    svg2png:
      build:
        files: [
          expand: true
          src: ['img_src/**/*.svg']
          ext: '.png'
          rename: (dest, src) -> src
        ]
    imagemin:
      build:
        files: [
          expand: true
          cwd: 'img/source/'
          src: ['**/*.{png,jpg,gif}']
          dest: 'img/'
        ]
    concat:
      footer:
        # Use array comprehension to add path prefix
        # There's probably a more idiomatic way to do this with Grunt, but I haven't figured it out
        # These scripts are added right before closing `body` tag
        src: ( "bower_components#{path}" for path in  ['/jquery/jquery.min.js'] )
        dest: 'js/components.js'
      head:
        # These scripts are added to `head`
        src: ( "bower_components#{path}" for path in  ['/modernizr/modernizr.js'] )
        dest: 'js/head-components.js'
    uglify:
      footer:
        src: ['js/components.js', 'js/main.js']
        dest: 'js/footer.js'
      head:
        src: ['js/head-components.js']
        dest: 'js/head.js'
    copy:
      # Copy jquery.min.map, for debugging in web tools (doesn't affect end users, but nice to have)
      jquery:
        src: ['bower_components/jquery/jquery.min.map']
        dest: 'js/jquery.min.map'
      # Copy svg files from img_src to img
      svg:
        files: [
          expand: true
          cwd: 'img_src/'
          src: ['**/*.svg']
          dest: 'img/'
        ]
      polyfills:
        src: ['bower_components/background-size-polyfill/backgroundsize.min.htc']
        dest: 'backgroundsize.min.htc'
    watch:
      css:
        files: ['scss/*.scss']
        tasks: ['compass:build']
      js:
        files: ['coffee/main.coffee']
        tasks: ['js']


  @loadNpmTasks 'grunt-contrib-compass'
  @loadNpmTasks 'grunt-contrib-coffee'
  @loadNpmTasks 'grunt-svg2png'
  @loadNpmTasks 'grunt-contrib-imagemin'
  @loadNpmTasks 'grunt-contrib-concat'
  @loadNpmTasks 'grunt-contrib-uglify'
  @loadNpmTasks 'grunt-contrib-copy'
  @loadNpmTasks 'grunt-contrib-watch'

  @registerTask 'js', ['coffee', 'concat', 'uglify']
  @registerTask 'build', ['compass', 'js', 'copy', 'svg2png', 'imagemin']
  @registerTask 'default', ['build']