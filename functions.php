<?php

	add_theme_support('post-formats');
	add_theme_support('post-thumbnails');
	add_theme_support('menus');

	add_filter('get_twig', 'add_to_twig');

	# Define useful constants for theme directories
	define('THEME_URL', get_template_directory_uri());
	define('JS_DIR', THEME_URL . '/js');
	define('CSS_DIR', THEME_URL . '/css');
	define('IMG_DIR', THEME_URL . '/img');


	# Add custom functions to Twig
	function add_to_twig($twig){
		/* this is where you can add your own fuctions to twig */
		$twig->addExtension(new Twig_Extension_StringLoader());
		$twig->addFilter('myfoo', new Twig_Filter_Function('myfoo'));
		return $twig;
	}

	# Example Twig filter
	function myfoo($text){
    	$text .= ' bar!';
    	return $text;
	}


	# Load JavaScript resources
	function load_scripts(){

		# Add scripts to footer, just before closing body tag
		wp_register_script( 'footer', JS_DIR . '/footer.js', array(), false, true );

		# Certain scripts need to be added to head (like Modernizr)
		wp_register_script( 'head', JS_DIR . '/head.js');

		# Enqueue scripts
		wp_enqueue_script('footer');
		wp_enqueue_script('head');
	}
	add_action('wp_enqueue_scripts', 'load_scripts');

	# Load CSS resources into wp_head
	function load_styles(){

		# Main style file, generated with Compass/Sass
		wp_register_style('main', CSS_DIR . '/main.css');

		wp_register_style('ie8', CSS_DIR . '/ie8.css');

		# Enqueue styles
		wp_enqueue_style('main');

		$GLOBALS['wp_styles']->add_data( 'ie8', 'conditional', 'lt IE 9' );
		wp_enqueue_style('ie8');
	}
	add_action('wp_enqueue_scripts', 'load_styles');

	# Create theme subclass
	class Lakeshore extends Timber {

		# Add stuff to context
		# Only add things that are needed in every template
		static function get_context() {
			# Get parent context
			$context = parent::get_context();

			# Create nested arrays to group related things
			$context['header'] = array();
			$context['footer'] = array();

			# Add CSS, Javascript, and image directories;
			$context['css_dir'] = IMG_DIR;
			$context['js_dir'] = JS_DIR;
			$context['img_dir'] = IMG_DIR;

			# Add primary navigation menu
			$context['header']['menu'] = new TimberMenu('primary');
			$context['header']['social_menu'] = new TimberMenu('social');

			# Add widgets
			$context['footer']['widgets'] = Timber::get_widgets('footer');

			# Return context with new stuff added
			return $context;
		}
	}

	# Register menus
	register_nav_menus(array(
					'primary' => 'Primary Navigation',
					'social' => 'Social Menu'
				) );


	# Register widget areas
	function add_widget_areas() {
		register_sidebar( array(
				'name' => 'Footer',
				'id' => 'footer',
				'description' => 'Widgets in this area are shown in the site footer',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h3 class="widget-title">',
				'after_title' => '</h3>',
			) );
	}
	add_action( 'widgets_init', 'add_widget_areas' );