(function() {
  var log;

  log = function(msg) {
    return typeof console !== "undefined" && console !== null ? console.log(msg) : void 0;
  };

  $(window).load(function() {
    return $('body').removeClass('preload');
  });

  $(function() {
    var $console, $email, $fields, $form, $message, $name, $nav, $navToggle, $social, $socialToggle, $submit, atLeastOneFieldEmpty, closeNav, closeSocial, isEmptyField, navIsClosed, openNav, openSocial, socialIsClosed;
    $nav = $('.primary-navigation');
    $social = $('.social-menu');
    $socialToggle = $('li.social > a', $nav);
    openNav = function() {
      return $nav.removeClass('closed');
    };
    openSocial = function() {
      $social.removeClass('closed');
      return $socialToggle.addClass('active');
    };
    closeNav = function() {
      return $nav.addClass('closed');
    };
    closeSocial = function() {
      $social.addClass('closed');
      return $socialToggle.removeClass('active');
    };
    closeNav();
    closeSocial();
    navIsClosed = function() {
      return $nav.hasClass('closed');
    };
    socialIsClosed = function() {
      return $social.hasClass('closed');
    };
    $navToggle = $('<a/>', {
      id: 'nav-toggle',
      href: '#'
    });
    $nav.before($navToggle);
    $navToggle.click(function(e) {
      e.preventDefault;
      if (navIsClosed()) {
        openNav();
      } else {
        closeNav();
      }
      return false;
    });
    $socialToggle.click(function(e) {
      e.preventDefault;
      if (socialIsClosed()) {
        openSocial();
      } else {
        closeSocial();
      }
      return false;
    });
    $form = $('#contact-form');
    $name = $('input[name=name]', $form);
    $email = $('input[name=email]', $form);
    $message = $('textarea[name=message]', $form);
    $fields = $name.add($email).add($message);
    $submit = $('input[name="submit"]', $form);
    $console = $('.console', $form);
    isEmptyField = function($el) {
      return $el.val() === "";
    };
    atLeastOneFieldEmpty = function() {
      return $fields.filter(function() {
        return isEmptyField($(this));
      }).size() > 0;
    };
    $submit.click(function(e) {
      var request;
      e.preventDefault();
      if (!atLeastOneFieldEmpty()) {
        $fields.removeClass('error');
        request = $.ajax({
          url: $form.attr('action'),
          type: 'post',
          data: $form.serialize()
        });
        return request.done(function(response, textStatus, jqXHR) {
          $console.html(response.message);
          return $fields.val("");
        });
      } else {
        return $fields.each(function() {
          if (isEmptyField($(this))) {
            return $(this).addClass('error');
          }
        });
      }
    });
    return $fields.blur(function() {
      if (!isEmptyField($(this))) {
        return $(this).removeClass('error');
      }
    });
  });

}).call(this);
