# Useful for debugging
log = (msg) -> console?.log msg

# jQuery stuff

$(window).load ->
  $('body').removeClass('preload')

$ ->
  #  ===================
  #  = Navigation Menu =
  #  ===================
  $nav = $('.primary-navigation')
  $social = $('.social-menu')

  $socialToggle = $('li.social > a', $nav)

  # Opens navigation
  openNav = -> $nav.removeClass('closed')
  # Opens social menu
  openSocial = ->
    $social.removeClass('closed')
    $socialToggle.addClass('active')

  # Closes navigation
  closeNav = -> $nav.addClass('closed')
  # Closes social menu
  closeSocial = ->
    $social.addClass('closed')
    $socialToggle.removeClass('active')

  # Close navigation and social menu on page load
  closeNav()
  closeSocial()

  # Returns true if navigation is closed
  navIsClosed = -> $nav.hasClass('closed')
  # Returns true if navigation is closed
  socialIsClosed = -> $social.hasClass('closed')

  # Create navigation toggle
  $navToggle = $('<a/>', id: 'nav-toggle', href: '#')
  $nav.before($navToggle)

  # Toggle navigation when navigation toggle is clicked
  $navToggle.click (e) ->
    e.preventDefault
    if navIsClosed() then openNav() else closeNav()
    false

  # Toggle social menu when menu item is clicked
  $socialToggle.click (e) ->
    e.preventDefault
    if socialIsClosed() then openSocial() else closeSocial()
    false

  #  ================
  #  = Contact Form =
  #  ================
  $form = $('#contact-form')

  $name = $('input[name=name]', $form)
  $email = $('input[name=email]', $form)
  $message = $('textarea[name=message]', $form)

  $fields = $name.add($email).add($message)

  $submit = $('input[name="submit"]', $form)

  $console = $('.console', $form)

  isEmptyField = ($el) -> $el.val() is ""
  atLeastOneFieldEmpty =  -> $fields.filter( -> isEmptyField($(this)) ).size() > 0

  $submit.click (e) ->
    e.preventDefault()

    unless atLeastOneFieldEmpty()
      $fields.removeClass('error')
      request = $.ajax
        url: $form.attr('action')
        type: 'post'
        data: $form.serialize()

      request.done (response, textStatus, jqXHR) ->
        $console.html(response.message)
        $fields.val("")

    else
      $fields.each -> $(@).addClass('error') if isEmptyField($(this))

  $fields.blur -> $(@).removeClass('error') unless isEmptyField($(this))