# Useful for debugging
log = (msg) -> console?.log msg

# jQuery stuff

$(window).load ->
  $('body').removeClass('preload')


$ ->
  $nav = $('.primary-navigation')
  $social = $('.social-menu')

  $socialToggle = $('li.social > a', $nav)

  # Opens navigation
  openNav = -> $nav.removeClass('closed')
  # Opens social menu
  openSocial = ->
    $social.removeClass('closed')
    $socialToggle.addClass('active')

  # Closes navigation
  closeNav = -> $nav.addClass('closed')
  # Closes social menu
  closeSocial = ->
    $social.addClass('closed')
    $socialToggle.removeClass('active')

  # Close navigation and social menu on page load
  closeNav()
  closeSocial()

  # Returns true if navigation is closed
  navIsClosed = -> $nav.hasClass('closed')
  # Returns true if navigation is closed
  socialIsClosed = -> $social.hasClass('closed')

  # Create navigation toggle
  $navToggle = $('<a/>', id: 'nav-toggle', href: '#')
  $nav.before($navToggle)

  # Toggle navigation when navigation toggle is clicked
  $navToggle.click (e) ->
    e.preventDefault
    if navIsClosed() then openNav() else closeNav()
    false

  # Toggle social menu when menu item is clicked
  $socialToggle.click (e) ->
    e.preventDefault
    if socialIsClosed() then openSocial() else closeSocial()
    false